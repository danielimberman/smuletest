# README #

Answer to the three questions given for Smule's coding challenge

## Build Versions

For this coding test I used Spark 1.6.0 with Scala 2.10.5.

## Building/Loading to Docker


This app was built using assembly, so to compile/package simply run
```
sbt clean assembly
```

Once you have the jar, simply build the image with the following command:
```
docker build . 
```

I personally chose to not use the sbt docker plugin, as I have had problems with integrating it with spark in the past.


## Running the Test

To run the jar locally, simply run the provided docker-compose file:

```bash
docker-compose up -d
```

This will run a local instance that already comes with the project jar at `/app/CodingExample.jar` and the provided dataset at `/app/data.tsv`.



Once the instance is running, use the following command to log in to the instance's bash shell:
```bash
docker exec -it smuletest_master_1 /bin/bash
```

Finally, run the spark-submit command. I have provided a pre-baked command that will read from `/app/data.tsv` and will write to console
```bash
 spark-submit --master local\
 	--num-executors 1\
 	--driver-memory 2g\
    --executor-memory 1g\
    --executor-cores 1\
    --class com.smule.datascience.CosineSimilarity\
    /app/CodingExample.jar\
```