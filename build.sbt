name := "SmuleTest"

version := "1.0"

scalaVersion := "2.11.5"


version := "1.0"

scalaVersion := "2.11.5"

libraryDependencies += "org.apache.spark" % "spark-core_2.11" % "1.6.0" % "provided"
libraryDependencies += "org.apache.spark" % "spark-mllib_2.11" % "1.6.0" % "provided"

libraryDependencies += "org.scalatest" % "scalatest_2.11" % "3.0.1"
libraryDependencies += "org.typelevel" % "cats_2.11" % "0.9.0"
libraryDependencies += "joda-time" % "joda-time" % "2.9.7"
libraryDependencies += "org.scalacheck" % "scalacheck_2.11" % "1.13.4"
libraryDependencies += "org.typelevel" % "cats-core_2.11" % "0.9.0"

mainClass in (Compile, run) := Some("com.smule.datascience.CosineSimilarity")