package com.smule.LogParsing

import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

/**
  * Created by danielimberman on 02/03/17.
  */

//Aug 15 00:00:00 a14.sf.smle.co snp 2016-08-15 00:00:00,000 [play-thread-49] INFO controllers - POST data
case class LogValue(date: DateTime, url: String, thread: String, logLevel: String, source: String, shard: Option[String], message: String)

object LogReader {

  private def parseDate(input: Array[String]) = {
    val dateString = input.slice(5, 7).mkString(" ").dropRight(4)
    val format = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")
    DateTime.parse(dateString, format)
  }

  private def parseURL(input: Array[String]) = input(3)

  private def parseThread(input: Array[String]) = input(7)

  private def parseLogLevel(input: Array[String]) = input(8)

  private def parseSource(input: Array[String]) = input(9)

  private def parseShard(input: Array[String]) = if (input(11).contains("shard")) Some(input(11)) else None

  private def parseMessage(input: Array[String]) = if (input(11).contains("shard")) input.slice(12, 14).mkString(" ") else input.slice(11, 13).mkString(" ")


  def run(input: List[String]) = {

    val validLogs = input.map(parseLog).flatMap {
      case Left(l) =>
        println(l)
        Seq()
      case Right(r) => Seq(r)
    }

    val shardsGroupedByHour = shardsByHour(validLogs)

    shardsGroupedByHour.foreach {
      case ((day, hour, shard), value) =>
        println(s"$hour, $shard, $value")
    }

  }


  def parseLog(input: String): Either[String, LogValue] = {
    try {
      val split = input.split(" ")
      val date = parseDate(split)
      val url = parseURL(split)
      val thread = parseThread(split)
      val source = parseSource(split)
      val logLevel = parseLogLevel(split)
      val shard = parseShard(split)
      val message = parseMessage(split)

      Right(LogValue(date, url, thread, logLevel, source, shard, message))
    }
    catch {
      case e: Exception => Left(e.getMessage)
    }

  }

  def shardsByHour(input: List[LogValue]) = {
    val valuesWithShards = input.filter(_.shard.nonEmpty)

    //group by day, hour, and shard
    val groupedByHour = valuesWithShards.groupBy(l => (l.date.toLocalDate, l.date.hourOfDay().get(), l.shard))
    val countPerHourAndShard = groupedByHour.mapValues(_.size)
    countPerHourAndShard.toList.sortBy { case ((day, hour, shard), _) => (hour, shard) }
  }


}
