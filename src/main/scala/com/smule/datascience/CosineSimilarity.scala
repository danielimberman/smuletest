package com.smule.datascience

import org.apache.spark.mllib.feature.{HashingTF, IDF}
import org.apache.spark.mllib.linalg.{SparseVector, DenseVector, Vectors}
import org.apache.spark.mllib.linalg.distributed.{MatrixEntry, RowMatrix}
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by danielimberman on 03/03/17.
  *
  * For this module I will calculate cosine similarity using basic LSH for optimization
  */
object CosineSimilarity extends App with Serializable{
  val secondsInDay = 84600
  val threshold = 0.7
  val numBuckets = 5
  val conf = new SparkConf().setAppName("CosineSimilarity")
  val sc = new SparkContext(conf)

  val tf = new HashingTF()
  // Load and parse the data file.
  val allInfo = sc.textFile("/app/input.tsv").map { line =>
    line.split(' ').toSeq
  }.map(t =>{
    val appxTime = t.last.toInt - (t.last.toInt % 6000)
    tf.transform(t.init :+ appxTime).toSparse
  })

  val anchors = allInfo.take(numBuckets)
  val groupedBuckets = allInfo.zipWithIndex().map{case(v,i) => (createBucketKey(v),(v,i))}.groupByKey()
  val similarities = groupedBuckets.values.flatMap(findSimilaritiesForBucket).filter(_._2 < threshold)


  similarities.collect().foreach(println)






  //split vectors into buckets
  def createBucketKey(a:SparseVector):String = {
    anchors
        .map(calculateCosineSimilarity(a, _))
        .map(x => if (x>=threshold)1 else 0)
        .foldLeft("")((s,b) => s+b)
  }





  //find similarities for a bucket in cartesian fashion
  def findSimilaritiesForBucket(input: Iterable[(SparseVector,Long)]) = {
    val indexed = input.toStream.zipWithIndex
    indexed.flatMap{
      case ((vector, i),ind) =>
        indexed.drop(ind).map{
          case((v2, j),_) =>
            ((i,j),calculateCosineSimilarity(vector,v2))
        }
    }

  }
  def calculateCosineSimilarity(a: SparseVector, b: SparseVector): Double = {
    val a1Norm = magnitude(a.values)
    val b1Norm = magnitude(b.values)
    val norm = a1Norm * b1Norm
    val dot = dotProduct(a.indices.zip(a.values), b.indices.zip(b.values))
    dot / norm

  }

  private def magnitude(a: Array[Double]) = math.sqrt(a.foldLeft(0.0)((av, bv) => av + bv * bv))

  private def dotProduct(a: Array[(Int, Double)], b: Array[(Int, Double)]): Double = {
    val aMap = a.toMap
    val bMap = b.toMap
    (aMap.keySet ++ bMap.keySet)
        .foldLeft(List[Double]())(
          (b, i) => {
            b :+ (aMap.getOrElse(i, 0.0) * bMap.getOrElse(i, 0.0))
          }
        ).sum
  }

}
