package com.smule.LogParsing

import org.joda.time.DateTime
import org.scalatest.{FlatSpec, Matchers}

/**
  * Created by danielimberman on 02/03/17.
  */
class LogReaderTest extends FlatSpec with Matchers{
  "parseLog" should "parse a log statement into a valid log object" in {
    val input = "Aug 15 00:00:00 a14.sf.smle.co snp 2016-08-15 00:00:00,000 [play-thread-49] INFO controllers - POST data"

    noException should be thrownBy LogReader.parseLog(input)

    val result= LogReader.parseLog(input)

    val dateTime = DateTime.parse("2016-08-15T00:00:00.000")
    result shouldEqual Right(LogValue(dateTime,"a14.sf.smle.co","[play-thread-49]","INFO","controllers",None, "POST data"))

  }

}
